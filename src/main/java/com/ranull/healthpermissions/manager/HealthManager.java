package com.ranull.healthpermissions.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ranull.healthpermissions.HealthPermissions;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.scheduler.BukkitRunnable;

public class HealthManager {
	private HealthPermissions plugin;

	public HealthManager(HealthPermissions plugin) {
		this.plugin = plugin;
	}

	public void secondTimer() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Player player : plugin.getServer().getOnlinePlayers()) {
					setMaxHealth(player);
				}
			}
		}.runTaskTimer(plugin, 0L, 20L);
	}

	public void setMaxHealth(Player player) {
		player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(getMaxHealth(player));
	}

	public int getMaxHealth(Player player) {
		List<Integer> healthPermissionList = new ArrayList<Integer>();

		for (PermissionAttachmentInfo perm : player.getEffectivePermissions()) {
			if (perm.getPermission().contains("healthpermissions.health.")) {
				try {
					healthPermissionList.add(Integer.parseInt(perm.getPermission()
							.replace("healthpermissions.health.", "")));
				} catch (NumberFormatException ignored) {
				}
			}
		}

		if (!healthPermissionList.isEmpty()) {
			return Collections.max(healthPermissionList);
		} else {
			return 20;
		}
	}
}
