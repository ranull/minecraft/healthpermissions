package com.ranull.healthpermissions;

import org.bukkit.plugin.java.JavaPlugin;

import com.ranull.healthpermissions.manager.HealthManager;

public class HealthPermissions extends JavaPlugin {
	@Override
	public void onEnable() {
		HealthManager health = new HealthManager(this);

		health.secondTimer();
	}
}
